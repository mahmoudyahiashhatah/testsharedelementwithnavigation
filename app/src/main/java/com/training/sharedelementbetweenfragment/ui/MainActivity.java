package com.training.sharedelementbetweenfragment.ui;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.navigation.ActivityNavigator;

import com.training.sharedelementbetweenfragment.R;
import com.training.sharedelementbetweenfragment.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityMainBinding binding = DataBindingUtil.setContentView(this,R.layout.activity_main);

        ActivityNavigator.applyPopAnimationsToPendingTransition(this);
    }
}

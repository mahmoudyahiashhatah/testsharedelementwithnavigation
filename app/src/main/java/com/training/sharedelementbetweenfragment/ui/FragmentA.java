package com.training.sharedelementbetweenfragment.ui;

import android.os.Bundle;
import android.transition.Slide;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.FragmentNavigator;

import com.training.sharedelementbetweenfragment.R;
import com.training.sharedelementbetweenfragment.databinding.FragmentABinding;

public class FragmentA extends Fragment {

    private NavController navController;

    FragmentABinding binding;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

         binding = DataBindingUtil.inflate(inflater, R.layout.fragment_a, container, false);



        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(view);
        binding.laA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Slide slide = new Slide();
                slide.setDuration(1000);
                FragmentA.this.setExitTransition(slide);

                FragmentNavigator.Extras extras = new FragmentNavigator.Extras.
                        Builder()
                        .addSharedElement(binding.ivTestB, "laA")
                        .build();

                navController.navigate(R.id.action_fragmentA_to_fragmentB,
                        null, // Bundle of args
                        null,
                        extras);// NavOptions

            }
        });
    }
}
